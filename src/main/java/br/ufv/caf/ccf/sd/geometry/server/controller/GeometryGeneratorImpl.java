package br.ufv.caf.ccf.sd.geometry.server.controller;

import br.ufv.caf.ccf.sd.geometry.common.model.Geometry;


import java.io.File;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;
import br.ufv.caf.ccf.sd.geometry.common.model.Position;
import br.ufv.caf.ccf.sd.geometry.common.model.Shape;

public class GeometryGeneratorImpl extends UnicastRemoteObject implements GeometryGenerator {
    public static void main(String[] args) {
        System.out.println();
        System.setProperty("java.security.policy","file://".concat(new File("").getAbsolutePath().concat("/src/all.policy")));
        try {
            GeometryGenerator geo = new GeometryGeneratorImpl();
            Server server = new Server(geo, "127.0.0.1", "GeometryGenerator");

            System.out.println("Started...");
            while (true) {
                Thread.yield();
                Thread.sleep(Integer.MAX_VALUE);
            }
        } catch (RemoteException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public GeometryGeneratorImpl() throws RemoteException {
        super();
    }

    @Override
    public Geometry generateGeometry() throws RemoteException {
        Random r = new Random();
        Shape[] s = Shape.values();
        Shape shape = s[r.nextInt(s.length)];
        ArrayList<Integer> p = new ArrayList<Integer>();
        int width, height, posx, posy;

        p.add(r.nextInt(768));
        p.add(r.nextInt(768));
        p.sort(Comparator.comparingInt(o -> (int) o));

        posx = p.get(0);
        width = p.get(1) - p.get(0);
        p.clear();
        p.add(r.nextInt(768));
        p.add(r.nextInt(768));
        posy = p.get(0);
        height = p.get(1) - p.get(0);

        Position pos = new Position(posx, posy);
        if(shape == Shape.SQUARE){
            width = height = Integer.min(width, height);
        }
        return new Geometry(shape,width,height,pos);
    }
}
