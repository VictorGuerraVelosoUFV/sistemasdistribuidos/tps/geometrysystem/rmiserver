package br.ufv.caf.ccf.sd.geometry.server.controller;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {
    private static final int PORT = 1098;

    Server(Remote remote, String ip, String name) throws RemoteException {
        super();
        init(remote,ip,name);
    }

    private void init(Remote remote, String ip, String name){
        try {
            Registry registry = LocateRegistry.createRegistry(PORT);
            String uri = "rmi://" + ip + ":" + PORT + "/" + name;
            System.out.println(uri);
            Naming.rebind(uri,remote);
        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }

}
